<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
require_once 'phpmailer/PHPMailerAutoload.php';

if (isset($_POST['name']) && isset($_POST['email']) ) {

 
    if (empty($_POST['name']) || empty($_POST['email']) ||  empty($_POST['phone'])) {
        $data = array('success' => false, 'message' => 'Please fill out the form completely. (A)'.$_POST['name']);
        echo json_encode($data);
        exit;
    }

    //create an instance of PHPMailer
    $mail = new PHPMailer();
    $mail->CharSet = 'UTF-8';
    $mail->From = $_POST['email'];
    $mail->FromName = $_POST['name'];
    $mail->AddAddress('luke.helg@softbank.ne.jp'); //recipient 
    $mail->Subject = '予約';
    $mail->Body = "Name: " . $_POST['name'] . "\r\n\r\nEmail: ". $_POST['email'] . "\r\n\r\nPh:". $_POST['phone'] .  "\r\n\r\nDate: ". $_POST['date'] .  "\r\n\r\nTime: ". $_POST['time']  . "\r\n\r\n人数: ". $_POST['number'] ."\r\n\r\nMessage: " . stripslashes($_POST['message']);

    if (isset($_POST['ref'])) {
        $mail->Body .= "\r\n\r\nRef: " . $_POST['ref'];
    }

    if(!$mail->send()) {
        $data = array('success' => false, 'message' => 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo);
        echo json_encode($data);
        exit;
    }

    $data = array('success' => true, 'message' => 'Thanks for your message homie!');
    echo json_encode($data);

} else {

    $data = array('success' => false, 'message' => 'Please fill out the form completely.(B)');
    echo json_encode($data);

}