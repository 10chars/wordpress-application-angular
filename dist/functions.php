<?php
function angular_scripts(){

	wp_register_script('angular', get_template_directory_uri().'/modules/angular/angular.min.js' );
	wp_enqueue_script('angular');
	wp_register_script('angular-route',get_template_directory_uri().'/modules/angular-route/angular-route.min.js');
	wp_enqueue_script('angular-route');
	wp_register_script('angular-module', get_template_directory_uri().'/modules/module.js');
	wp_enqueue_script('angular-module');	
	wp_localize_script( 'angular-module', 'bloginfo', load_bloginfo());
	
}
add_action('init','angular_scripts');

function load_bloginfo(){

    $array = array();
    $array['baseUrl'] = get_bloginfo('url');
    $array['templateUrl'] = get_bloginfo('template_url');
    $array['pluginsUrl'] = plugins_url();
    return $array;

}
add_theme_support( 'post-thumbnails' ); 

function include_custom_posts($allowed_post_types) {

	$allowed_post_types[] = 'item';
	
	return $allowed_post_types;

}
add_filter( 'rest_api_allowed_post_types', 'include_custom_posts');

function add_meta_to_json($data, $post, $context) {

	$data['price']  = get_post_meta( $post['ID'], 'item_price', true );	
	$data['eigo']  = get_post_meta( $post['ID'], 'en_desc', true );
	$data['pretty_date'] = get_the_date('', $post['ID']);
	$data['pretty_time'] = get_the_time('', $post['ID']);

	
	return $data;
}
add_filter( 'json_prepare_post', 'add_meta_to_json' , 10, 3 );

function add_responsive($content){

    return preg_replace('/<img class="/', '<img class="img-responsive ', $content); //TODO: hacky-redo

}
add_filter('the_content','add_responsive');

remove_filter ('the_content',  'wpautop');
?>