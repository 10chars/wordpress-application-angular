angular.module('wpAngular', [])
.constant('wpBloginfo', bloginfo)
.run(function($rootScope, $log, wpBloginfo){
	/*Set path info for angular-route*/
	$rootScope.bloginfo = wpBloginfo;
});