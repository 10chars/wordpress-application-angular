var gulp = require('gulp');

var jshint = require('gulp-jshint');
var minifyHTML = require('gulp-minify-html');
var changed = require('gulp-changed');
var autoprefix = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var concat = require('gulp-concat');
var less = require('gulp-less');
var watch = require('gulp-watch');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var sourcemaps = require('gulp-sourcemaps');
var imagemin = require('gulp-imagemin');
var pngcrush = require('imagemin-pngcrush');
var path = require('path');
var distLocation = '../test';


gulp.task('jshint', function() {
  gulp.src('./scripts/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

 
gulp.task('htmlmin', function() {  
  gulp.src('./views/*.html')
    .pipe(changed('./views/*.html'))
    .pipe(minifyHTML())
    .pipe(gulp.dest('../dist/views'));
});

gulp.task('libcss', function() {
  gulp.src(['./lib/**/*.css'])
    .pipe(concat('libcss.css'))
    .pipe(minifyCSS()) 
    .pipe(gulp.dest('../dist/lib/css/'));
});

gulp.task('less', function(){
	gulp.src('./includes/less/*.less')
  .pipe(watch(function(files){
    return files.pipe(less()).pipe(autoprefix('last 2 versions')).pipe(minifyCSS()).pipe(gulp.dest('../dist'));
  }));
});

gulp.task('views', function(){
  gulp.src('./views/*.html')
  .pipe(gulp.dest('../dist/views/'));
});

gulp.task('libjs', function () {
  gulp.src([
    './lib/pickadate/picker.js',
    './lib/pickadate/picker.date.js',
    './lib/pickadate/picker.time.js',
    './lib/slicknav/jquery.slicknav.js'])
    .pipe(concat('lib.js'))
    .pipe(uglify())
    .pipe(gulp.dest('../dist/lib/js'));
});

gulp.task('js', function(){
  gulp.src([
    './modules/angular-animate/angular-animate.js',
    './modules/angular-cookies/angular-cookies.js',
    './modules/angular-resource/angular-resource.js',
    './modules/angular-sanitize/angular-sanitize.js',
    './modules/angular-touch/angular-touch.js',
    './modules/ng-google-maps/angular-google-maps.js',
    './modules/angular-easyfb.js',
    './scripts/**/*.js'])
    .pipe(concat('app.min.js'))
    .pipe(ngAnnotate())
    .pipe(sourcemaps.write())
    .pipe(uglify())
    .pipe(gulp.dest('../dist/app/'));

});

gulp.task('images', function () {
    return gulp.src('./images/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngcrush()]
        }))
        .pipe(gulp.dest('../dist/images/'));
});

gulp.task('php', function(){
  gulp.src([
    './functions.php',
    './index.php'])
    .pipe(gulp.dest('../dist'));

});

gulp.task('watch', function () {
  gulp.watch('./scripts/*.js', ['js']);
  gulp.watch('./views/*', ['views']);
  gulp.watch('./*.php', ['php']);

});


gulp.task('build', function() {
    gulp.start('styles', 'scripts', 'images');
});






