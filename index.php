<!DOCTYPE html>
<html lang="en" xmlns:ng="http://angularjs.org" id="ng-app" ng-app="wpAngularApp">
  <head>
    <meta charset="utf-8">
    <meta name="fragment" content="!">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Creek</title>

    <!-- TODO: merge and minify -->
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>/style.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>/lib/css/libcss.min.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400,300' rel='stylesheet' type='text/css'>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/2.0/animate.min.css" rel="stylesheet">

  
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head(); ?>

  </head>
  <body>
  
    <nav id="mobile-nav">
        <ul id="menu">
          <li><a href="/">ホーム</a></li>
           <li><a href="/menu">メニュー</a></li>
           <li><a href="/blog">ブログ</a></li>
           <li><a href="/reservation">ネット予約</a></li>
           <li><a href="/courses">コース</a></li>
           <li><a href="/access">アクセス</a></li>
           
      </ul>
      <div id="mobile-logo"><div class="logo-sm"></div></div> 
      </nav>

      <nav id="main-nav">
        <div class="container">
          <div class="col-sm-4" id="nav-logo">
            <div class="col-sm-5">
               <img src="<?php echo get_template_directory_uri(); ?>/images/creek-logo.png" alt="logo">
            </div>
          </div>
          <div class="col-sm-6 col-sm-offset-2" id="nav-items">       
            <div class="col-sm-2 col-sm-offset-2 nav-item">
              <a href="/">
              <div class="menu-icon"><i class="fa fa-home"></i></div>
              <div class="menu-label">home</div>
              </a>
            </div>
            <div class="col-sm-2 nav-item">
              <a href="/menu">
              <div class="menu-icon"><i class="fa fa-spoon"></i></div>
              <div class="menu-label">menu</div>
              </a>
            </div>
            <div class="col-sm-2 nav-item">
              <a href="/blog">
              <div class="menu-icon"><i class="fa fa-pencil"></i></div>
              <div class="menu-label">blog</div>
              </a>
            </div>
            <div class="col-sm-2 nav-item">
              <a href="/reservation">
              <div class="menu-icon"><i class="fa fa-calendar-o"></i></div>
              <div class="menu-label">reserve</div>
              </a>
            </div>
            <div class="col-sm-2 nav-item">
              <a href="/access">
              <div class="menu-icon"><i class="fa fa-map-marker"></i></div>
              <div class="menu-label">access</div>
              </a>
            </div>       
          </div>
      </nav>

      <div class="view" ng-view autoscroll="true"></div>
      
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.underscore.min.js"></script> 
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/js/lib.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/app/app.min.js"></script>
   
    <script>
      $(function(){
        $('#menu').slicknav({
          closeOnClick: true,
          label: ''
        });
      });
    </script>

    <?php wp_footer(); ?>
    
  </body>
</html>