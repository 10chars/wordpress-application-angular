angular.module('wpAngularApp', [
  'ngRoute',
	'wpAngular',
  'ngSanitize',
  'google-maps',
  'ngAnimate',
  'ezfb'
])
.run(function (ezfb) {
  ezfb.init({
    appId: '532857810151555'
  });
})
.config(function($routeProvider, wpBloginfo, $locationProvider, ezfbProvider) {
  $locationProvider.html5Mode(true);
  $locationProvider.hashPrefix('!');
   
  $routeProvider
  .when('/', {
    templateUrl: wpBloginfo.templateUrl + '/views/home.html',
    controller:'MainCtrl'
  })
  .when('/menu', {
    templateUrl: wpBloginfo.templateUrl + '/views/menu.html',
    controller: 'MainCtrl'
  })
  .when('/blog', {
    templateUrl: wpBloginfo.templateUrl + '/views/blog.html',
    controller: 'MainCtrl'
  })
  .when('/access', {
    templateUrl: wpBloginfo.templateUrl + '/views/access.html',
    controller: 'AccessCtrl'
  })
  .when('/info', {
    templateUrl: wpBloginfo.templateUrl + '/views/info.html',
    controller: 'MainCtrl'
  })
  .when('/reservation', {
    templateUrl: wpBloginfo.templateUrl + '/views/reservation.html',
     controller: 'ReserveCtrl'
  })
  .when('/courses', {
    templateUrl: wpBloginfo.templateUrl + '/views/courses.html',
     controller: 'MainCtrl'
  })
  .when('/:slug', {
    templateUrl: wpBloginfo.templateUrl + '/views/post.html',
    controller: 'SinglePostCtrl'
  })
  .otherwise({
    redirectTo: '/'
  });

});


