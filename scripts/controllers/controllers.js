'use strict';

angular.module('wpAngularApp')
  .controller('MainCtrl', function($scope, $location, $http, $sce, wpBloginfo, $window) {
  	$scope.tempURL = wpBloginfo.templateUrl;
    $scope.menuItems = [];
    var goodThingsA = ['times','people', 'food', 'music'];
    $scope.goodA = goodThingsA[Math.floor(Math.random() * 4)];
    var goodThingsB = ['vibes', 'wine', 'service'];
    $scope.goodB = goodThingsB[Math.floor(Math.random() * 3)];
    //Store data in browser session memory to limit http requests to one per visit.
    //TODO: Ditch sessionStorage for localStorage and set time limit for items that aren't updated frequently (e.g menu items).
    //TODO: http request in background on first page load and set to rootScope.
    //TODO: hook into WP-Post so author doesn't need to close the tab to see new posts and changes.
    
    // If nothing in sessionStorage, request database from server, else use the one in stored browser memory.
    if(sessionStorage.getItem("menuData") === null){
    console.log('No menu found in sessionStorage');  
    $scope.loading = true;
    //Retrieve WP database as JSON Object  	
  	$http.get('http://dev.creek.ng/wp-json/posts/?type=item').success(function(data) {
    $scope.menuItems = data;
    $scope.loading = false;
    sessionStorage["menuData"] = JSON.stringify(data);
    console.log('Menu saved to sessionStorage!');
    });
   }else{
    console.log('Found data sessionStorage');
    $scope.menuItems = JSON.parse(sessionStorage["menuData"]); 
   }

   if(sessionStorage.getItem("starterData") === null){
    console.log('No menu found in sessionStorage');  
    $scope.loading = true;
    //Retrieve WP database as JSON Object   
    $http.get('http://dev.creek.ng/wp-json/posts/?type=starter').success(function(data) {
    $scope.starters = data;
    $scope.loading = false;
    sessionStorage["starterData"] = JSON.stringify(data);
    console.log('Starter saved to sessionStorage!');
    });
   }else{
    console.log('Found starters sessionStorage');
    $scope.starters = JSON.parse(sessionStorage["starterData"]); 
   }

  	
    $scope.blogPosts = [];
    $scope.start = 0;
    $scope.end = 3;
    $scope.hasNextPosts = true;
    $scope.hasPrevPosts = false;
    // If nothing in sessionStorage, request database from server, else use the one in browser memory.
    if(sessionStorage.getItem("blogData") === null){
    console.log('No blog data found in sessionStorage');   
    $scope.loadingBlog = true; 	
  	$http.get('http://dev.creek.ng/wp-json/posts/').success(function(data) {        
    $scope.blogPosts = data;
    sessionStorage["blogData"] = JSON.stringify(data);
    $scope.loadingBlog = false;
    console.log('Saved blog data to sessionStorage!');
    $scope.paged = $scope.blogPosts.slice($scope.start,$scope.end);
    });
    }else{
      console.log('Found data sessionStorage');
      $scope.blogPosts = JSON.parse(sessionStorage["blogData"]);
      $scope.paged = $scope.blogPosts.slice($scope.start,$scope.end); 
    }


    $scope.nextPage = function(direction){
      if(direction === 1){
      $window.scrollTo(0,0);  
      $scope.start += 3;
      $scope.end += 3;
      console.log('FORWARDS');
      console.log('start: ' + $scope.start + ' end: ' + $scope.end);
      console.log('numofposts: ' + $scope.blogPosts.length);
      if($scope.end >= $scope.blogPosts.length){
        $scope.hasNextPosts = false;
        $scope.hasPrevPosts = true;
        $scope.paged = $scope.blogPosts.slice($scope.start,$scope.end);
        return;
      }  
      $scope.paged = $scope.blogPosts.slice($scope.start,$scope.end);
      $scope.hasPrevPosts = true;
    
    }else if (direction === -1){
      $window.scrollTo(0, 0);
      $scope.start -= 3;
      $scope.end -= 3;
      console.log('BACKWARDS');
      console.log('start: ' + $scope.start + ' end: ' + $scope.end);
      console.log('numofposts: ' + $scope.blogPosts.length);
     if($scope.start === 0){
        $scope.hasPrevPosts = false;
        $scope.hasNextPosts = true;
        $scope.paged = $scope.blogPosts.slice($scope.start,$scope.end);
        return;
      }
      $scope.paged = $scope.blogPosts.slice($scope.start,$scope.end);
      $scope.hasPrevPosts = true;
      $scope.hasNextPosts = true;
    }

    }

})
.controller('AccessCtrl', function($scope, $location, $http, $sce) { 
 
    //TODO: add styled maps functionality to module.    
    $scope.map = {
      center: {
        latitude: 34.706693,
        longitude: 137.728707
      },
      zoom: 17
	   };

     $scope.marker = {
          id:0,
          coords: {
                latitude: 34.706693,
                longitude: 137.728707}
      };

})

.controller('ReserveCtrl', function($scope, $location, $http, $sce, wpBloginfo) {

  $scope.tempURL = wpBloginfo.templateUrl;

    $scope.result = 'hidden';    
    $scope.resultMessage;
    $scope.formData = {}; 
    $scope.submitButtonDisabled = false;
    $scope.submitted = false;

    $scope.submit = function(contactform) {
        
        $scope.submitted = true;
        $scope.submitButtonDisabled = true;
        if (contactform.$valid) {
            $http({
                method  : 'POST',
                url     : $scope.tempURL + '/contact-form.php',
                data    : $.param($scope.formData),  
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).success(function(data){
                console.log(data);
                if (data.success) {
                    $scope.submitButtonDisabled = true;
                    $scope.resultMessage = 'Thanks yo! We\'ll be in touch';
                    $scope.result='bg-success';
                } else {
                    $scope.submitButtonDisabled = false;
                    $scope.resultMessage = 'Please check your shit.';
                    $scope.result='bg-danger';
                }
            });
        } else {
            $scope.submitButtonDisabled = false;
            $scope.resultMessage = 'Failed :( Please fill out all the fields.';
            $scope.result='bg-danger';
        }
    }
   
})


.controller('SinglePostCtrl', function($scope, $location, $http, $sce, $routeParams) {
 
  var uri = 'http://dev.creek.ng/wp-json/posts/?filter[name]=' + $routeParams.slug;
  console.log('QUERY = ' + $routeParams.slug);
  $http.get(uri).success(function(data) {   
    console.log('success - title: ' + data);
    $scope.post = data[0];
    });     
});