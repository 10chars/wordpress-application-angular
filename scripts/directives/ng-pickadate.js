angular.module('wpAngularApp').directive('datepicker', function() {
    return {
      require: 'ngModel',
      restrict: 'CA',

        link: function(scope, element, ngModel) {              
           element.on('click', function() { 
                       
                $(element).pickadate({
                  onSet: function(){
                    scope.formData.date = $(element).val();
                  }
                });
            });         
        }
    };
});



angular.module('wpAngularApp').directive('timepicker', function() {
    return {
      restrict: 'CA',
      link: function(scope, element) {
        element.on('click', function() {
          $(element).pickatime({
                  onSet: function(){
                    scope.formData.time = $(element).val();
                  }
                });
        });
      }
    }
});


